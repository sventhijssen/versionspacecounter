# VersionSpaceCounter

## How to use
You can use this software for the project assignment of AI to calculate the lengths of the G-set and S-set.
Copy-paste the output of the SWI-Prolog learning between the quotes of the variable versionSpace and run.
The sizes will be printed for each iteration.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

* [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.txt)
* [License](license.txt)