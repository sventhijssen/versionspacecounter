import re

"""
Created by Sven Thijssen
7 November 2016, Leuven
-----------------------

You can use this software for the project assignment of AI to calculate the lengths of the G-set and S-set.
Copy-paste the output of the SWI-Prolog learning between the quotes of the variable versionSpace and run.
The sizes will be printed for each iteration.

------------------------

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.
"""

versionSpace = """

Current list of examples : [[almadrie,breakfast,friday,cheap]:p,[demoete,lunch,friday,expensive]:n,[almadrie,lunch,saturday,cheap]:p,[sedes,breakfast,sunday,cheap]:n,[almadrie,breakfast,sunday,expensive]:n]

Start chrono...
Initializing...
Processing...
The G-set is : [[alma_top,meal_top,day_top,cost_top]]
The S-set is : [[alma_bottom,meal_bottom,day_bottom,cost_bottom]]

Reading next example : [almadrie,breakfast,friday,cheap] with classification : p
Processing...
The G-set is : [[alma_top,meal_top,day_top,cost_top]]
The S-set is : [[almadrie,breakfast,friday,cheap]]

Reading next example : [demoete,lunch,friday,expensive] with classification : n
Processing...
The G-set is : [[alma_top,breakfast,day_top,cost_top],[alma_top,meal_top,day_top,cheap],[almadrie,meal_top,day_top,cost_top]]
The S-set is : [[almadrie,breakfast,friday,cheap]]

Reading next example : [almadrie,lunch,saturday,cheap] with classification : p
Processing...
The G-set is : [[alma_top,meal_top,day_top,cheap],[almadrie,meal_top,day_top,cost_top]]
The S-set is : [[almadrie,meal_top,day_top,cheap]]

Reading next example : [sedes,breakfast,sunday,cheap] with classification : n
Processing...
The G-set is : [[almadrie,meal_top,day_top,cost_top]]
The S-set is : [[almadrie,meal_top,day_top,cheap]]

Reading next example : [almadrie,breakfast,sunday,expensive] with classification : n
Processing...
The G-set is : [[almadrie,meal_top,day_top,cheap]]
The S-set is : [[almadrie,meal_top,day_top,cheap]]

...All examples learned
The G-set is : [[almadrie,meal_top,day_top,cheap]]
The S-set is : [[almadrie,meal_top,day_top,cheap]]
Stop chrono... Repeated 1 time(s)...
Used total time : 0 milliseconds.
Used total inferences : 8070

"""

regexG = "The\sG-set\sis\s:\s\[([\[\]\w_,]+)\]"
regexS = "The\sS-set\sis\s:\s\[([\[\]\w_,]+)\]"
patternG = re.compile(regexG)
patternS = re.compile(regexS)
findG = re.findall(patternG, versionSpace)
findS = re.findall(patternS, versionSpace)

print "Number of elements in G"
print "-----------------------"
for (lists) in findG:
    currentList = lists.split(',[')
    print len(currentList)

print "\n"

print "Number of elements in S"
print "-----------------------"
for (lists) in findS:
    currentList = lists.split(',[')
    print len(currentList)